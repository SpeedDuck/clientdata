# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-06-12 00:25-0300\n"
"PO-Revision-Date: 2018-03-05 15:31+0000\n"
"Language-Team: Polish (https://www.transifex.com/akaras/teams/959/pl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pl\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);\n"

#. (itstool) path: texts/text
#: texts.xml:7
msgid "Attack"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:8
msgid "Stop"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:9
msgid "Watch Out"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:10
msgid "Heal me"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:11
msgid "Follow me"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:12
msgid "Retreat"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:15
msgid "Hello"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:16
msgid "Goodbye"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:17
msgid "Thank you"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:18
msgid "Good job"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:19
msgid "I need help"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:20
msgid "Go away"
msgstr ""

#. (itstool) path: texts/text
#: texts.xml:21
msgid "Speak to this NPC"
msgstr ""
