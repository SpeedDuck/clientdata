<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="castle_lightsources" tilewidth="32" tileheight="96" tilecount="12" columns="4">
 <image source="castle_lightsources.png" width="128" height="288"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="150"/>
   <frame tileid="4" duration="150"/>
   <frame tileid="8" duration="150"/>
   <frame tileid="4" duration="150"/>
  </animation>
 </tile>
 <tile id="1">
  <animation>
   <frame tileid="1" duration="150"/>
   <frame tileid="5" duration="150"/>
   <frame tileid="9" duration="150"/>
  </animation>
 </tile>
 <tile id="2">
  <animation>
   <frame tileid="2" duration="150"/>
   <frame tileid="6" duration="150"/>
   <frame tileid="10" duration="150"/>
  </animation>
 </tile>
 <tile id="3">
  <animation>
   <frame tileid="3" duration="150"/>
   <frame tileid="7" duration="150"/>
   <frame tileid="11" duration="150"/>
   <frame tileid="7" duration="150"/>
  </animation>
 </tile>
</tileset>
